<?php
/*
	Plugin Name: Lista de Sliders
	Description: Muestra una lista de sliders
	Author: Carlos Escobar
	Author URI: http://www.weblabor.mx
	Version: 1.0

	Licenced under the GNU GPL:

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// If this file is called directly, then abort execution.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once plugin_dir_path( __FILE__ ) . 'includes/class-sliders-list-manager.php';


function run_sliders_list_manager() {
	$mct = new Sliders_List_Manager();
	$mct->run();
}

run_sliders_list_manager();

function get_sliders($quantity) {
	$r = new WP_Query( array(
	   'posts_per_page' => $quantity,
	   'no_found_rows' => true, /*suppress found row count*/
	   'post_status' => 'publish',
	   'post_type' => 'slider',
	   'ignore_sticky_posts' => true,
	) );
	if (!$r->have_posts()) {
		return [];
	}

	$count = -1;
	$sliders = [];
	while ( $r->have_posts() ) : $r->the_post(); 
		$count++;
		$class = '';
		if($count==0) {
			$class = ' active';
		}
		$link = get_post_meta(get_the_ID(), 'wsl_link', true);
		if(strlen($link)==0) {
			$link = '#';
		}
		$url = get_the_post_thumbnail_url(null, 'full');
		$slider = [
			'link'  => $link,
			'url'   => $url,
			'count' => $count,
			'class' => $class
		];
		$sliders[] = $slider;
	endwhile;
	// Reset the global $the_post as this query will have stomped on it
	wp_reset_postdata();
	return $sliders;
}

function show_sliders($quantity=5, $type='bootstrap') {
	$images = get_sliders($quantity);
	include 'includes/'.$type.'.php';
}
?>
