<?php

/**
 * This class defines all functionality for the dashboard
 * of the plugin
 *
 * @package WSLM
 */

class WSL_Custom_Type {
    
    public function create_post_type() {
        register_post_type( 'slider',
            array(
                'labels' => array(
                    'name' => __( 'Sliders' ),
                    'singular_name' => __( 'Slider' )
                ),
                'public' => true,
                'has_archive' => false,
                'supports'=> array("title","author","thumbnail"),
                'taxonomies' => ['post_tag']
            )
        );
    }

}
