<?php

/**
 * Class for making the metabox called Year
 *
 * @package WSLM
 */

class WSL_Metabox {
    
    private $id;
    private $title;
    private $screen = "slider";
    private $context;
    private $priority = "default";

    public function __construct($loader, $id, $title, $context = 'normal') {
    	$loader->add_action("admin_init", $this, "create");
    	$loader->add_action('save_post', $this, 'save');
    	$this->id = $id;
    	$this->title = $title;
    	$this->context = $context;
    }

    public function create() {
    	add_meta_box( $this->id, $this->title, array($this, 'display'), $this->screen, $this->context, $this->priority);
    }

    public function display() {
    	global $post;
	  	$field = get_post_meta($post->ID, 'wsl_'.$this->id, true);
	  	wp_nonce_field( 'wsl_'.$this->id.'_meta_box_nonce', 'wsl_'.$this->id.'_meta_box_nonce' );

	  	?>
	 		<input type="text" class="widefat" name="<?= $this->id; ?>" value="<?php if($field != '') echo esc_attr( $field ); ?>" />
	   
	  	<?php
    }

    public function save($post_id) {
    	if ( ! isset( $_POST['wsl_'.$this->id.'_meta_box_nonce'] ) ||
	  	! wp_verify_nonce( $_POST['wsl_'.$this->id.'_meta_box_nonce'], 'wsl_'.$this->id.'_meta_box_nonce' ) )
	    	return;
	  
	  	if (!current_user_can('edit_post', $post_id))
	    	return;
	  
	  	$old = get_post_meta($post_id, 'wsl_'.$this->id, true);
	  	$new = $_POST[''.$this->id];

		if ( !empty( $new ) && $new != $old )
			update_post_meta( $post_id, 'wsl_'.$this->id, $new );
		elseif ( empty($new) && $old )
		    delete_post_meta( $post_id, 'wsl_'.$this->id, $old );
    }

}
