<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php foreach($images as $image) { ?>
            <li data-target="#carouselExampleIndicators" data-slide-to="<?= $image['count'] ?>" class="<?= $image['class'] ?>"></li>
        <?php } ?>
    </ol>
    <div class="carousel-inner">
        <?php foreach($images as $image) { ?>
            <div class="carousel-item <?= $image['class'] ?>">
                <a href="<?= $image['link'] ?>"><img src="<?= $image['url'] ?>" alt="<?= $image['description'] ?>" class="d-block w-100"></a>
            </div>
        <?php } ?>
    </div>
    <?php if(count($images) > 1) { ?>
        <!-- Controls -->
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    <?php } ?>
</div>