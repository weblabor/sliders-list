<?php
/**
 * @package    WSLM
 */

class Sliders_List_Manager {

    protected $loader;

    public function __construct() {
        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->load_metaboxes();
    }
   
    private function load_dependencies() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'modules/loader.php'; // Load modules classes
        require_once plugin_dir_path( __FILE__ ) . 'class-sliders-list-manager-loader.php';
        $this->loader = new Sliders_List_Manager_Loader();
    }
    
    private function define_admin_hooks() {
        $customType = new WSL_Custom_Type();
        $this->loader->add_action( 'init', $customType, 'create_post_type' );
    }

    private function load_metaboxes() {
        new WSL_Metabox($this->loader, 'link', 'Enlace');
    }
    
    public function run() {
        $this->loader->run();
    }

}