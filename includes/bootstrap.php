<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php foreach($images as $image) { ?>
            <li data-target="#carousel-example-generic" data-slide-to="<?= $image['count'] ?>" class="<?= $image['class'] ?>"></li>
        <?php } ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php foreach($images as $image) { ?>
            <div class="item <?= $image['class'] ?>">
                <a href="<?= $image['link'] ?>"><img src="<?= $image['url'] ?>" alt="<?= $image['description'] ?>" style="width:100%;"></a>
                <div class="carousel-caption">
                    <?= $image['description'] ?>
                </div>
            </div>
        <?php } ?>
    </div>

    <?php if(count($images) > 1) { ?>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    <?php } ?>
</div>
